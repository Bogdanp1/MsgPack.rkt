.. default-role:: code

###############################
 Release history and changelog
###############################

This document tracks the changes for each release. Please annotate every new
release with the date and follow semantic versioning. If the changes are minor
it suffices to list then in a bulleted list, but for larger changes please
elaborate in prose. In the case of breaking changes a migration guide should be
provided.


Version 0.3.0
#############
:date: 2018-11-09

This release breaks the previous version: the procedures `pack` and `unpack`
are changed, `pack-to`, `unpack-from` and `unpack/rest` are added.

Breaking changes:

- `pack` takes in any number of objects and returns a byte string of packed
  data
- `unpack` takes in a byte string of packed data and returns the first unpacked
  object

New:

- `pack-to` takes in an output port and any number of objects, it writes the
  packed data to the port
- `unpack-from` takes in an input port and unpacks one object
- `unpack/rest` is similar to `unpack`, except it returns to values: the
  unpacked object and the remaining packed bytes

To migrate from `0.2` or earlier replace `pack` with `pack-to` and make the
port the first argument instead of the second one, and replace `unpack` with
`unpack-from`. You can also replace the workaround for getting a byte string:

.. code-block:: racket

   ;; Replace these
   (call-with-output-bytes
     (λ (in)
       (pack some-object-to-pack)))
   (call-with-input-bytes some-byte-string
     (λ (out)
       (unpack some-byte-string)))

   ;; with these
   (pack some-object-to-pack)
   (unpack some-byte-string)
